module Main exposing (Model, Msg(..), ResultS(..), init, main, subscriptions, update, view)

import Browser
import Html exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Decode exposing (Decoder, field, string)
import Task
import Time



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type ResultS
    = Failure
    | Loading
    | Success String
    | Stopped


type alias UrlWMsg =
    { nameUrl : String
    , url : String
    , message : UrlMsg
    , r : ResultS
    }


type alias Model =
    { listUrls : List UrlWMsg
    , runAjax : Bool
    , zone : Time.Zone
    , time : Time.Posix
    , tickTime : Float
    }


fromUrlSmallToUrlBig : String -> Int -> String
fromUrlSmallToUrlBig a i =
    a ++ String.fromInt i


newHttpOrder : Model -> Int -> Cmd Msg
newHttpOrder m i =
    if m.runAjax then
        Cmd.batch (listUrlsToHttpGet m.listUrls i)

    else
        Cmd.none


resultDecoder : Decoder String
resultDecoder =
    field "title" string


listUrlsConsult : List UrlWMsg
listUrlsConsult =
    [ UrlWMsg "Albums" "https://jsonplaceholder.typicode.com/albums/" GotResultA Stopped
    , UrlWMsg "Photos" "https://jsonplaceholder.typicode.com/photos/" GotResultPh Stopped
    , UrlWMsg "Photos" "https://jsonplaceholder.typicode.com/photos/" GotResultPh Stopped
    , UrlWMsg "Photos" "https://jsonplaceholder.typicode.com/photos/" GotResultPh Stopped
    , UrlWMsg "Photos" "https://jsonplaceholder.typicode.com/photos/" GotResultPh Stopped
    , UrlWMsg "Photos" "https://jsonplaceholder.typicode.com/photos/" GotResultPh Stopped
    ]


listUrlsToHttpGet : List UrlWMsg -> Int -> List (Cmd Msg)
listUrlsToHttpGet l i =
    List.map
        (\a ->
            Http.get
                { url = fromUrlSmallToUrlBig a.url i
                , expect = Http.expectJson (UrlFin a.message) resultDecoder
                }
        )
        l


init : () -> ( Model, Cmd Msg )
init _ =
    let
        iniUrs =
            listUrlsConsult
    in
    ( Model iniUrs False Time.utc (Time.millisToPosix 0) 1000
      -- , newHttpOrder 1
    , Cmd.none
    )



-- UPDATE


type UrlMsg
    = GotResultA
    | GotResultPh


type Msg
    = UrlFin UrlMsg (Result Http.Error String)
    | Tick Time.Posix
    | AdjustTimeZone Time.Zone
    | ChangeAjaxState


procesResultList : List UrlWMsg -> UrlMsg -> Result Http.Error String -> List UrlWMsg
procesResultList l m res =
    let
        pR =
            case res of
                Ok fullText ->
                    Success fullText

                Err _ ->
                    Failure
    in
    List.map
        (\j ->
            if j.message == m then
                { j | r = pR }

            else
                j
        )
        l


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlFin m result ->
            ( { model | listUrls = procesResultList model.listUrls m result }, Cmd.none )

        Tick newTime ->
            let
                whichOne =
                    Time.toSecond model.zone model.time
            in
            ( { model | time = newTime }
            , newHttpOrder model whichOne
            )

        AdjustTimeZone newZone ->
            ( { model | zone = newZone }
            , Cmd.none
            )

        ChangeAjaxState ->
            let
                l2 =
                    List.map
                        (\s ->
                            if not model.runAjax then
                                { s | r = Loading }

                            else
                                { s | r = Stopped }
                        )
                        model.listUrls
            in
            ( { model | runAjax = not model.runAjax, listUrls = l2 }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every model.tickTime Tick



-- VIEW


ajaxActivate : Bool -> Html Msg
ajaxActivate j =
    if j then
        text "Ajax Active"

    else
        text "Ajax Inactive"


apiResultToText : ResultS -> Html Msg
apiResultToText m =
    case m of
        Failure ->
            text "I was unable to load your book."

        Loading ->
            text "Loading..."

        Success fullText ->
            pre [] [ text fullText ]

        Stopped ->
            text "Stopped"


view : Model -> Html Msg
view model =
    let
        hour =
            String.fromInt (Time.toHour model.zone model.time)

        minute =
            String.fromInt (Time.toMinute model.zone model.time)

        second =
            String.fromInt (Time.toSecond model.zone model.time)

        structsResults =
            List.map
                (\a ->
                    div
                        []
                        [ h3 [] [ text a.nameUrl ]
                        , p [] [ apiResultToText a.r ]
                        ]
                )
                model.listUrls
    in
    div []
        ([ h1 []
            [ text "Main Page" ]
         , button [ onClick ChangeAjaxState ] [ ajaxActivate model.runAjax ]
         , h2 [] [ text "Now" ]
         , div [] [ pre [] [ text (hour ++ ":" ++ minute ++ ":" ++ second) ] ]
         ]
            ++ structsResults
        )
